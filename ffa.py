'''
Program which calculates the maximum flow of a given graph (read from file - see below for example file, 2 available formats) using the Ford-Fulkerson algorithm.
It prints the current flow and residual graph at each iteration using netzorkx; and can render the final flow using graphviz.
Uses numpy, matplotlib and scipy (for the simplex algorithm), networkx (for rendering) and graphviz (for image output)

Coded using jupyter notebook


Our goal was to find an optimal distribution of trains between two cities on the French railroad network
'''

'''


NB: For step-by-step rendering, the graph position is determined randomly. As such, execute the program several times for different results.
See code below for the customization of additional parameters.
(toggle the rendering of the graph at every iteration, toggle final image output, toggle to show it)
'''

import numpy as np
import matplotlib.pyplot as plt

import networkx as nx
import graphviz

import time


# residual capacity
def res_cap(gph, u, v):
    return gph[u][v]['capacity'] - gph[u][v]['flow']
    
'''
Rendering functions
'''

# Auxiliary function, format: 'flow/capacity'
def print_flow(d):
    if d['capacity'] == 0:
        return d['flow']
    return str(d['flow'])+'/'+str(d['capacity'])

# Graph rendering function: takes the layout as parameter, and the graph color (used to differentiate residual graph and flow network)

def renderGraph(G, pos, color):
    # node rendering
    nx.draw_networkx_nodes(G, pos, node_color='#A0CBE2', node_size=300)
    # shows node labels
    nx.draw_networkx_labels(G, pos, font_family='sans-serif')
    
    # show existing flow edges
    edges_flow = [(u, v) for (u, v, d) in G.edges(data=True) if d['flow'] > 0]
    nx.draw_networkx_edges(G, pos, edgelist=edges_flow,
                           width=3, alpha=0.5, edge_color=color)
    # show their labels
    flow_slash_cap = {(u, v): print_flow(d) for (u, v, d) in G.edges(data=True) if d['flow'] > 0}
    nx.draw_networkx_edge_labels(G, pos,font_family='sans-serif', font_color=color, font_weight='bold', label_pos = 0.33, edge_labels = flow_slash_cap)


    
    # show edges without flow
    slash_cap = {(u, v):'0/'+str(d['capacity']) for (u, v, d) in G.edges(data=True) if d['capacity'] > 0 and d['flow'] == 0}
    nx.draw_networkx_edges(G, pos, edgelist=slash_cap,
                           width=3, alpha=0.5, edge_color='black')
    # show their labels
    nx.draw_networkx_edge_labels(G, pos, font_family='sans-serif', font_color='black', font_weight='bold', label_pos = 0.33, edge_labels = slash_cap)

    # Render using matplotlib
    plt.axis('off')
    plt.show()
    

# Step-by-step rendering of the network and its residual graph
def printStep(i, gph, pos):
    # print network in green
    print('Flow ({})'.format(i))
    renderGraph(gph, pos, 'g')
    
    # print residual grpah in blue
    print('Residual ({})'.format(i))
    resid = nx.DiGraph()
    for u,v in gph.edges:
        if res_cap(gph, u, v) > 0:
            # to avoid rendering useless edges, we set c = 0
            resid.add_edge(u,v,capacity=0, flow =res_cap(gph,u,v))
                
    renderGraph(resid, pos, 'b')

'''
Image output using graphviz (and dot)
The result is saved as xxx.png
Tested only with jupyter
'''

def writeGraph(gph, labels, graph_filename, view = False):
    dot = graphviz.Digraph(filename=graph_filename)
    
    dot.attr('node', color='red')
    dot.node(B[s])
    dot.node(B[t])
    dot.attr('node', color = 'black')
    
    for u,v in gph.edges:
        f = gph[u][v]['flow']
        if f > 0:
            dot.edge(labels[u], labels[v], label=str(f))

    dot.render(view=view, format='png')

    
'''
Ford-Fulkerson algorithm (using a DFS)
'''   

# find augmenting path
def findAugmentingPath(gph, s, t):
    # parent stores the index of the node that led to this noode's traversal
    # it is a dictionary: we do not have to preoccupy ourselves with networkx's internals
    parent, stack = {}, [s]
    
    # by convention, the soruce is of parent -1
    parent[s] = -1
    
    # We apply a DFS to find a path
    while stack != []:
        u = stack.pop()  
        
        # found sink: no need to continue
        if u == t: 
            break
        
        # traverse neighbors
        for v in gph.neighbors(u):
            # ignore nodes that are not accessible
            if res_cap(gph, u, v) == 0:
                continue
                
            # only add nodes that have not been marked
            if not v in parent:
                parent[v] = u
                stack.append(v)
    
    # no path in the residual graph
    if t not in parent:
        return None
    
    # else, generate path in reverse order
    path = [t]
    # end at the source
    while parent[path[-1]] != -1:
        path.append(parent[path[-1]])
    
    # we have to reverse the path 
    return path[::-1]


# calculate flow traversing path
def calcFlow(gph, path):
    n = len(path)
    
    # take the minimal capacity of all links
    cap = res_cap(gph, path[0], path[1])
    for i in range(1, n-1):
        cap = min(cap, res_cap(gph, path[i], path[i+1]))
    # small test
    assert(cap > 0)
    
    return cap
    
    
# Apply flow to the network
def applyFlow(gph, flow):
    for u,v,f in flow:
        gph[u][v]['flow'] += f
        # flow symetry
        gph[v][u]['flow'] -= f
        
        # verification: capacity constraints
        assert(gph[u][v]['flow'] <= gph[u][v]['capacity'])

        
# Main algorithm
def findMaxFlow(G, s, t, drawStep = True, output = False, labels = [], graph_name='graph1', viewOutput = False):
    # To avoid side-effects
    gph = G.copy()
            
    # Render step by step
    pos = nx.spring_layout(gph) # To impose graph position between each step
    
    maxflow = 0
    i = 0 # iteration number
    
    while True:
        # render network and residual graph
        if drawStep:
            printStep(i, gph, pos)
        
        # Find aumenting path
        path = findAugmentingPath(gph, s, t)
        
        # No more augmenting path: by the maxflow-mincut theorem, we have a maximum flow!
        if path == None:
            break
        
        # Calculate flow to add
        flux = calcFlow(gph, path)
        # Translate it into flow
        flow = [(path[i],path[i+1],flux) for i in range(len(path)-1)]
        # Add it
        applyFlow(gph, flow)
        
        maxflow += flux
        i += 1
        
    if output:
        writeGraph(gph, labels, graph_name, viewOutput)
        
    return maxflow


'''
Read input function from file

File format:
First line: number of nodes, source id, sink id
Next lines: u,v,capacity
example: test_ford.txt

8 0 3
0 4 10
0 1 10
1 2 10
2 6 10
2 5 1
4 5 10
5 7 10
6 3 10
7 3 10
'''

def readFile(filename):
    with open(filename) as file:
        # first line
        first = map(int,file.readline().split())
        n,s,t = first
        
        A, B = [], [str(i) for i in range(n)] # A = edges, B = name of nodes
        
        # read lines ony by one
        for line in file:
            # read edge: in node, out node, then capacity
            u,v,c = [int(s) for s in line.split(' ')]
            # add to the list of edges
            A.append((u,v,c))
    
        # print(A) # debugging
    
        return A, s, t, n, B

'''
Function to read the French railroad map from a file (that we had named france.txt)
The goal was to get the maximum flow between two cities s and t ( numbers given in the last line of the file)

First line: number of nodes, number of edges, source, sink
m lines after: u,v,length,speed (from our modelisation of the network)
n lines after:  node, name of the node (e.g. name of the city)
'''

def readMap(mapname):
    with open(mapname) as file:
        first = map(int,file.readline().split())
        n,m,s,t = first # n = n nodes,m = n edges, s = source, t = sink
        A, B = [], n*[''] # A = edges, B = string of the node names

        for line in file:
            tokens = line.split()
            if len(tokens) == 4:
                u,v,lon,vit = map(float,tokens)
                # in out modelisation, we only take into account speed
                A.append((int(u),int(v),vit)) 
                A.append((int(v),int(u),vit))
                
            elif len(tokens) == 2:
                u,name = int(tokens[0]), tokens[1]
                B[u] = '{}({})'.format(u,name)
            else:
                raise RuntimeError('Corrupt file')
        
        return A, s, t, n, B


'''
Main program
Draws maximum flow of a network read froma file
Decomment corresponding line (depending on chosen format)
'''

# import warnings
# warnings.filterwarnings('ignore')

# Lecture du graphe
A, s, t, n, B = readFile('test_ford.txt')
# A, s, t, n, B = readMap('france.txt')

# graph construction
G = nx.DiGraph()
for u,v,c in A:
    G.add_edge(u, v, capacity=c, flow=0)

    
# normalisation of the graph
# We add edges (v,u) for each (u,v) if it does not yet exist

##################################################
# /!\ Depending on the goal, we may want to add a null capacity edge (directed graph)
# or copy edge in the other way (for undirected graph)
##################################################

for u,v in G.edges:
    if not G.has_edge(v,u):
        G.add_edge(v,u,capacity=0,flow=0)

# Execution of the algorithm
print('flow = ', findMaxFlow(G, s, t, True, True, B, 'graph1', True))   # CHANGE THESE VALUES

# Decomment following lines to drawStep the flow from jupyter
'''
from graphviz import Source
Source.from_file('graph1')
'''
