# Presentation

The presentation I gave for my TIPE examination on the maxflow-mincu theorem and its applications to flow algorithms is accessible in examples/presentation.pdf 
It is in French however.

The project is divided into two files:

- ffa.py: application of the maxflow-mincut theorem to maxflow algorithms - implementation of the Edmonds-Karp algorithm to solve general maxflow problems

- graphcut.py: application of the maxflow-mincut theorem to image segmentation through the graphcut algorithm

# From ffa.py: Maximum flow generation and visualisation

Step-by-step rendering of flow and residual graph

![picture](examples/Image2.png)
![picture](examples/Image3.png)

Final output of maximum flow

![picture](examples/example-dot-render.png)

# From graphcut.py: Image segmentation

![picture](examples/camel.png)
![picture](examples/camel_scribbled.png)
![picture](examples/camel-res.png)

![picture](examples/deer.png)
![picture](examples/deer_scribbled.png)
![picture](examples/deer-res.png)

![picture](examples/faucong.png)
![picture](examples/faucong_scribbled.png)
![picture](examples/faucong-res.png)
