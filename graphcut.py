'''
Program which segments an image into a foreground and background using the graphcut algorithm.
Uses the EdmondsKarp or Dinic algorithm to compute the mincut of the graph.
Uses sparse matrices to save memory on larger images.
Uses code from https://sandipanweb.wordpress.com/2018/02/11/interactive-image-segmentation-with-graph-cut/ for mathematical computations

Uses numpy, matplotlib and scipy
Coded using jupyter
'''


import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import skimage.color as sk_color
from collections import defaultdict
import numpy as np
from collections import deque

# for sparse structures
from scipy.sparse import *  

'''
Implementation of two flow algorithms: Edmonds-Karp and Dinic using Python classes

To use numpy matrices, decomment the lines corresponding to NUMPY
To use sparse lil matrices (using its rows method), decomment the lines corresponding to SPARSE_LIL.
To use sparse lil matrices (using the nonzeros method), decomment the lines corresponding to SPARSE.

They can be found in the following sections:
- one occurence in EdmondsKarp.applyAugmentingPath and in Dinic.calcLevelGraph
- two occurences in Graphcut: constructNetwork et calcMinCut
'''

class EdmondsKarp:    
    def calcMaxFlow(self, gphe, s, t):
        # the residual graph
        self.resid = gphe.copy()
        # source and sink
        self.s, self.t = s, t
        self.maxflow = 0

        i = 0
        while True:
            # find augmenting path
            flow = self.applyAugmentingPath()
            self.maxflow += flow
            
            i += 1
            # debugging
            if i % 250 == 0:
                print(round(self.maxflow,3), end=" ")
                
            if flow == 0:
                break
        
        # print final flow
        print(round(self.maxflow,3), end=" ")
        
        return self.maxflow
    
    # return augmenting path between s an t (returns a list of encountered nodes)
    def applyAugmentingPath(self):
        # EdmondsKarp: BFS
        # As for FordFulkerson, 
        # parent denotes the index of the node which has got to the node in question
        parent, queue = {}, deque()
        
        # we start the traversal from s
        queue.append(self.s)

        # the root does not have any parents so by convention we set it to -1
        parent[self.s] = -1

        while queue:
            u = queue.popleft()
            
            # we add u neighbors to the queue, and we mark them as checked
            
            # SPARSE_LIL
            for v in self.resid[u].rows[0]: 
            # SPARSE
            #for v in self.resid[u].nonzero()[1]: 
            # NUMPY
            #for v in np.nonzero(self.resid[u])[0]: 
                if (not v in parent):
                    parent[v] = u
                    queue.append(v)
                    
            # we stop at the first found path
            if self.t in parent:
                break

        # no paths to t
        if not (self.t in parent):
            return 0

        # reconstruct the path: it ends at t
        u = self.t
        cap = float("inf")

        # continue up to s
        while parent[u] != -1:
            cap = min(cap, self.resid[parent[u], u])
            u = parent[u]

        # apply the flow to the graph: we have to take care to reverse the path we have found
        u,v = self.t, parent[self.t] # v denotes u's parent in the established path (inverse order)
        
        while v != -1:
            self.resid[v,u] -= cap
            self.resid[u,v] += cap # property of flow symetry
            
            # sme verifications
            assert(self.resid[u,v] >= 0 and self.resid[v,u] >= 0)
            
            u,v = v, parent[v]

        return cap
    
                        
class Dinic:
    def calcMaxFlow(self, gphe, s, t):
        self.resid = gphe.copy()
        self.s, self.t = s, t
        self.maxflow = 0
        self.n = gphe.shape[0]
        
        # Instead of using a deque to represent a queue, we use a fixed-size array because we know it will hold at most n elements
        self.queue = np.zeros(self.n, dtype=int)
        
        while True:
            print('flow = ', round(self.maxflow,3))
            
            # distance to the source s, initialized at -1
            self.d = np.full(self.n, -1, dtype=int)
            
            # neighbors to traverse
            self.neigh = [[] for i in range(self.n)]
            
            # index of the next neighbor to traverse from given node (see Dinic algorithm, and applyFlow function)
            self.ptr = np.zeros(self.n, dtype=int)
            
            # calculate neighbors and levels
            self.calcLevelGraph()
            
            # if the sink is not accessible
            if self.d[self.t] < 0:
                break
                
            while self.applyFlow(self.s, float("inf")) != 0:
                pass
                
        print('flow = ', round(self.maxflow,3))
        
        return self.maxflow
                    
    def calcLevelGraph(self):
        # 'static' queue as seen before
        i,m = 0,1 # current index and size of queue
        self.queue[0] = self.s
        
        # initialization 
        self.d[self.s] = 0
        
        # while the queue is not empty
        while i < m:
            u = self.queue[i]
            i += 1
            
            # no need to continue
            if self.d[self.t] != -1 and self.d[u] >= self.d[self.t]:
                break
            
            # traverse neighbors
            
            # SPARSE_LIL
            for v in self.resid[u].rows[0]: 
            # SPARSE
            #for v in self.resid[u].nonzero()[1]: 
            # NUMPY
            #for v in np.nonzero(self.resid[u])[0]: 
            
                # not yet traversed
                if self.d[v] == -1: 
                    self.d[v] = self.d[u] + 1
                    self.queue[m] = v
                    m += 1
                
                # we only want neighbors whose levels are strictly higher
                if self.d[v] == self.d[u]+1: 
                    self.neigh[u].append(v)

                    
    # apply flow according to the Dinic algorithm
    def applyFlow(self, u, c):
        if c == 0:
            return 0
        
        # found the sink
        if u == self.t:
            # print('+{}'.format(c), end=' ')
            self.maxflow += c
            return c
        
        # alias
        neigh = self.neigh[u]
        n = len(neigh)
        
        while self.ptr[u] < n:
            v = neigh[self.ptr[u]]

            # see Dinic algorithm
            cap = self.applyFlow(v, min(c,self.resid[u,v]))
            
            if cap == 0:
                self.ptr[u] += 1
                continue
                
            self.resid[u,v] -= cap
            self.resid[v,u] += cap
            
            return cap
        
        return 0
    
'''
Graphcut Algorithm
See https://sandipanweb.wordpress.com/2018/02/11/interactive-image-segmentation-with-graph-cut/ for more details
The goal of this projects was not to study the probabilistic theory behind but only to apply the maxflow-mincut theorem in real-case scenarios.
As such, we reuse some snippets of his code.

A basic explanation: 
The goal is to isolate in an image a foreground from a background; using the original image and an image that we have marked beforehand with some pixels of each layer. 
We then translate the problem in terms of a mincut problem and use maxflow-mincut algorithms to find it.
Gaussian distributions are used to model the probability for a certain pixel to be part of the foreground / background according to its yuv code (as described in sandipanweb.com); this is then used to generate the network's capacities.
'''

# utility function: round x with a 2^(-p) precision
def round2(x, p):
    p2 = 1 << p
    return round(p2 * x) / p2
        
def gaussian(v, sigma):
    I = np.linalg.norm(v)**2
    x = np.exp(-I/2/(sigma**2))
    return x



class GraphCut:
    # rgb denotes the original rgb image, rgb_s the marked rgb image
    # yuv corresponds to another color space where the notion of distance is more "natural"
    def construct(self, image, image_s, precision, sigma):
        self.rgb = mpimg.imread(image)[:,:,:3]
        self.rgb_s = mpimg.imread(image_s)[:,:,:3]
        
        self.yuv = sk_color.rgb2yuv(self.rgb)
        self.yuv_s = sk_color.rgb2yuv(self.rgb_s)
        
        self.calcPdf()
        self.constructNetwork(precision, sigma)
       
        
    # calculate the network's important parameters ( in order to calculate probabilities, see sandipanweb)
    def calcPdf(self):
        print('[*] Probabilities calculation...')
        # code snippet fromm sandipanweb
        # separately store background and foreground scribble pixels in the dictionary comps
        comps = defaultdict(lambda:np.array([]).reshape(0,3))

        scribbles = np.nonzero(self.yuv != self.yuv_s) # marked pixels
        
        for i,j,_ in np.transpose(scribbles):
            color = tuple(g.yuv_s[i,j,:])
            # scribble color as key of comps
            comps[color] = np.vstack([comps[color], g.yuv[i,j,:]])
    
        self.mu, self.sigma = {}, {}
        
        
        # compute MLE parameters for Gaussians
        for c in comps:
            self.mu[c] = np.mean(comps[c], axis=0)
            self.sigma[c] = np.cov(comps[c].T)
            print('[>] Pour c =', c, ', mu =',self.mu[c])
        
        # debugging
        # There are 2 elements in comps: foreground and background
        for i, c in enumerate(comps):
            if i == 0:
                self.fc = c
                print('[>] fc =', self.fc)
            else: 
                self.bc = c
                print('[>] bc =', self.bc)
        

    def constructNetwork(self, precision, sigma):
        print('[*] Construction of the network...')
        w, h = self.rgb.shape[0], self.rgb.shape[1]
        
        # take into account source and sink
        self.n_nodes = w*h + 2
        print("[>] n =", w*h+2)
        
        # by convention, the source is at index n-2 and sink n-1
        self.s, self.t = self.n_nodes - 2, self.n_nodes - 1
        
        # construction de la matrice
        
        # SPARSE_LIL
        self.cap = lil_matrix((self.n_nodes, self.n_nodes))
        # SPARSE
        #self.cap = lil_matrix((self.n_nodes, self.n_nodes))
        # NUMPY
        #self.cap = np.zeros((self.n_nodes, self.n_nodes))
        
        # by convention, every pixel has 4 neighboring cells ( ecluding s and t )
        neighbors = [(-1,0),(1,0),(0,-1),(0,1)] 
        
        # see sandipanweb for more theoretical details
        for x in range(w):
            for y in range(h):
                # finding node's index
                i = x*h + y
                
                # adding edges corresponding to neighbors
                for a in neighbors:
                    p = (x + a[0],y + a[1])
                    
                    # only add connections from existing nodes
                    if 0 <= p[0] < w and 0 <= p[1] < h:
                        j = p[0]*h + p[1]
                        
                        self.cap[i,j] = round2(gaussian(self.yuv[x,y]-self.yuv[p], sigma), precision)
                        
                # add edge linking i to s and t
                if (self.yuv[x,y] != self.yuv_s[x,y]).any(): # if it is a marked pixel
                    if (self.yuv_s[x,y] == self.fc).all(): # if it is part of the foreground
                        self.cap[self.s,i] = float("inf")
                    if (self.yuv_s[x,y] == self.bc).all(): # if it is part of the background
                        self.cap[i,self.t] = float("inf")
                else: # if it has not been marked
                    p_f = gaussian(self.mu[self.fc]-self.yuv[x,y], sigma)
                    p_b = gaussian(self.mu[self.bc]-self.yuv[x,y], sigma)
                    self.cap[i,self.t] = round2(-np.log10(p_b / (p_f + p_b)), precision)
                    self.cap[self.s,i] = round2(-np.log10(p_b / (p_f + p_b)), precision)
        
    # debugging: draws image ( tested only with jupyter )
    def drawRgb(self):
        plt.imshow(self.rgb)
    
    def calcMinCut(self, method):
        print('[*] f =', method.calcMaxFlow(self.cap, self.s, self.t))
        print('[*] Calculating min cut...')
        
        # image segmentation result
        res = np.full(self.rgb.shape, self.bc)
        
        # height
        h = self.rgb.shape[1]
                    
        # SPARSE_LIL
        stack = list(method.resid[self.s].rows[0])
        # SPARSE
        #stack = list(method.resid[self.s].nonzero()[1])
        # NUMPY
        #stack = list(method.resid[self.s].nonzero()[0])        
        
        # see maxflow-mincut theorem for finding the mincut frorm the maxflow 
        while stack != []:
            u = stack.pop()
            
            x,y = u//h, u%h
        
            # do not go over the same pixel twice
            if u == self.s or u == self.t or (res[x,y] == self.fc).all():
                continue
            
            res[x,y] = self.fc
            
            # SPARSE_LIL
            for v in method.resid[u].rows[0]:
            # SPARSE
            #for v in method.resid[u].nonzero()[1]:
            # NUMPY
            #for v in method.resid[u].nonzero()[0]:
                stack.append(v)
                
        return sk_color.yuv2rgb(res)

'''
Function to load and construct network from image

Ability to change precision values (capacities will be brought to the nearest 2^(-p) ); as well as sigma (the width of the gaussian curve)
and name (name of the - 64x64 or a little more - image in the form of xxx.png and xxx_scribbled.png)
'''

precision, sigma = 2, 0.05 # precision of 0.25
name = 'xxx' # name of the image without extension: xxx.png et xxx_scribbled.png

g = GraphCut()

# construction of the equivalent network
# separation of the flow algorithm and the graphcut algorithm to be able to change flow algorithm without having to reload image (see jupyter)
g.construct(name + '.png', name + '_scribbled.png', precision, sigma)

# debugging in jupyter: draw original image
g.drawRgb()

'''
Main function
Executes flow algorithm and generation of the cut
Draws segmented image and execution time
Change flow algorithm: Decomment corresponding line
'''

import time

t0 = time.time()
#img = g.calcMinCut(EdmondsKarp())
img = g.calcMinCut(Dinic())
t1 = time.time()

print('[*] Time:', t1-t0)

# Draw segmented image
plt.imshow(img)
